<?php

namespace App;

class Helper{
    public static function isMobile($str){
        if(substr($str, 0, 2)!='05') return false;
        for($i=0; $i<strlen($str); $i++){
            if(!is_numeric($str[$i])) return false;
        }
        return strlen($str)==10;
    }
}