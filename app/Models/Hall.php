<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    use HasFactory;
    protected $appends = ['user', 'images'];

    public function getUserAttribute(){
        return User::where('id', $this->userId)->first();
    }

    public function getImagesAttribute(){
        return Gallery::where('hallId', $this->id)->get();
    }
}
