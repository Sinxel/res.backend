<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Hall;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{
    public function monthList($hall, $year, $month){
        $days = Carbon::create($year, $month);
        $dim = $days->daysInMonth;

        $rows = Event::whereRaw('YEAR(startDate)=?', $year)
                ->whereRaw('MONTH(startDate)=?', $month)
                ->where('hallId', $hall)
                ->where('isConfirmed', 1)->get();
        $events = [];
        for($i=0; $i<$dim; $i++){
            $events[$i] = 0;
        }
        foreach($rows as $row){
            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $row->startDate);
            $key = $startDate->format('d');
            $dv = $key-1;
            $events[$dv]++;
        }
        return response(['events'=>$events]);
    }

    public function dayEvents($id, $day){
        $rows = Event::where('hallId', $id)->whereDate('startDate', $day)->get();
        $events = [];
        foreach($rows as $row){
            $events[] = (object)array(
                'start'=>$row->startDate,
                'end'=>$row->endDate
            );
        }
        return response(['events'=>$events]);
    }
    public function reserve(Request $request){
        $day = $request->day;
        
        $day = substr($day, 0, 10);
        $startDate = Carbon::createFromFormat('Y-m-d', $day);
        $startDate->hour = $request->time;
        $startDate->minute = 0;
        $endDate = $startDate->clone();
        $endDate->addHours($request->duration);
        $event = new Event();
        $event->userId = $request->user;
        $event->hallId = $request->id;
        $event->startDate = $startDate;
        $event->endDate  = $endDate;
        $event->isConfirmed = false;
        $event->save();
        
        return response(['id'=>$event->id]);
    }

    public function getCount(Request $request){
        $user = $request->user();
        //عدد الحجوزات الغير مؤكدة التي تنتمي لجميع القاعات التي يملكها المستخدم صاحب القاعة
        $total = DB::select(DB::raw("SELECT COUNT(e.id) as total FROM events e INNER JOIN halls h ON e.hallId=h.id WHERE h.userId = :userId AND e.isConfirmed=0"), ['userId'=>$user->id]);
        
        return response(['total'=>$total[0]->total]);
        
    }

    public function getEvents(Request $request){
        $user = $request->user();
        $rows = DB::select(DB::raw("SELECT e.*  FROM events e INNER JOIN halls h ON e.hallId=h.id WHERE h.userId = :userId AND e.isConfirmed =0"), ['userId'=>$user->id]);
        $events = [];
        foreach($rows as $row){
            $events[] = (object)array(
                'id'=>$row->id,
                'user'=>User::where('id', $row->userId)->first(),
                'hall'=>Hall::where('id', $row->hallId)->first(),
                'startDate'=>$row->startDate,
                'endDate'=>$row->endDate,
            );
        }
        return response(['events'=>$events]);
    }

    public function reject($id){
        Event::where('id', $id)->delete();
        return response(['status'=>'ok']);
    }

    public function accept($id){
        $event = Event::where('id', $id)->first();
        $event->isConfirmed = true;
        $event->save();
        return response(['status'=>'ok']);
    }
}
