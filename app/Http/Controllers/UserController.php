<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UserController extends Controller
{
    public function authenticate(Request $request){
        $bearer = $request->header('authorization');
        $bearerInfo = explode(' ', $bearer);
        $token = $bearerInfo[1];
        $user = User::where('api_token', $token)->first();
        if($user==null){
            return response(['message'=>'Invalid Token'], 401);
        }
        return response(['user'=>$user]);
    }

    public function login(Request $request){
        $mobile  = $request->mobile;
        $password = $request->password;

        $user = User::where('mobile', $mobile)->first();
        if($user==null){
            return response(['message'=>'This user does not exist'], 404);
        }
        $passwordCheck = Hash::check($password, $user->password);
        if($passwordCheck){
            $user->api_token =  $user->createToken('Reser')->accessToken;
            $user->save();
            return response(['user'=>$user]);
        }
        return response(['message'=>'Invalid password'], 401);
    }

    public function register(Request $request){
        $user = new User();
        $user->mobile = $request->mobile;
        
        if(!Helper::isMobile($user->mobile)){
            return response(['message'=>'Invalid mobile number'], 422);
        }
        if(strlen(trim($request->password))<6){
            return response(['message'=>'Password must be 6 characters long'], 422);
        }
        if(strlen(trim($request->name))<3){
            return response(['message'=>'الإسم يجب أن يتكون من 3 أحرف على الأقل'], 422);
        }
        $user->name = $request->name;
        $user->password = Hash::make($request->password);
        $user->api_token = $user->createToken('Reser')->accessToken;
       
        $user->save();
        return response(['user'=>$user]);
    }
}
