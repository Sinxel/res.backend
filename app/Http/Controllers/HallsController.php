<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Hall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HallsController extends Controller
{
    public function create(Request $request){
        $name = trim($request->name);
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $address = $request->address;
        $phone = $request->phone;
        $capacity = $request->capacity;
        $comments = $request->comments;
        $email = $request->email;
        $priceRange = $request->priceRange;
        $discount = $request->discount;

        $hall = new Hall();
        $hall->userId = Auth::user()->id;
        $hall->name = $name;
        $hall->latitude = $latitude;
        $hall->longitude = $longitude;
        $hall->address = $address;
        $hall->phone = $phone;
        $hall->capacity = $capacity;
        $hall->priceRange = $priceRange;
        $hall->comments = $comments;
        $hall->discount = $discount;
        $hall->email = $email;
        $hall->save();
        if($hall->id>0){
            $images = $request->files;
            $counter = 0;
            foreach($images as $image){
                $counter++;
                $time = time();
                $date = date('Y-m-d');
                $userId = $hall->userId;
                $imageName = $date.'_'.$time.'_'.$userId.'_'.$counter;
                $extension = $image->getClientOriginalExtension();
                
                $path = Storage::putFileAs('/public', $image, $imageName.'.'.$extension); //save to server disk and return path
                $galleryItem = new Gallery();
                $galleryItem->hallId = $hall->id;
                $galleryItem->imagePath = $path;
                $galleryItem->save();
            }
        }else{
            return response(['error'=>'لم نتمكن من حفظ القاعة'], 503 );
        }
        
        return response(['hall'=> $hall]);
    }

    public function list(){
        //احصل على جميع القاعات المحفوظة في قاعدة البيانات
        return response(['halls'=>Hall::all()]);
    }

    public function hall($id){
        
        $hall = Hall::where('id', $id)->first();
        return response(['hall'=>$hall]);
    }

    public function search( $query){
        
        $halls = Hall::where('name', 'like', '%'.$query.'%')->orWhere('address', 'like', '%'.$query.'%')->orWhere('comments', 'like', '%'.$query.'%')->get();
        return response(['halls'=>$halls]);
    }

    public function delete($id){
        Hall::where('id', $id)->delete();
        return response(['result'=>1]);
    }

    public function home(){
        $halls = Hall::where('discount', '>', 0)->orderByRaw('RAND()')->limit(5)->get();
        return response(['halls'=>$halls]);

    }
}
