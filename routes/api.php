<?php

use App\Http\Controllers\EventsController;
use App\Http\Controllers\HallsController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

*/


Route::post('register', [UserController::class, 'register']);
Route::post('login', [UserController::class, 'login']);
Route::get('auth', [UserController::class, 'authenticate']);
Route::get('halls', [HallsController::class, 'list']);
Route::get('halls/{query}', [HallsController::class, 'search']);
Route::get('home', [HallsController::class, 'home']);
Route::get('hall/{id}', [HallsController::class, 'hall']);
Route::get('events/{hall}/{year}/{month}', [EventsController::class, 'monthList']);
Route::get('dayEvents/{id}/{day}', [EventsController::class, 'dayEvents']);

Route::middleware('auth:api')->group(function(){
    Route::post('halls/add', [HallsController::class, 'create']);
    Route::post('halls/reserve', [EventsController::class, 'reserve']);
    Route::get('user/reservations', [EventsController::class, 'getCount']);
    Route::get('user/events', [EventsController::class, 'getEvents']);
    Route::get('event/reject/{id}', [EventsController::class, 'reject']);
    Route::get('event/accept/{id}', [EventsController::class, 'accept']);
});



