<!doctype html>
<html lang="en">
  <head>

    <base href="/"/>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="/scripts/master.css" rel="stylesheet">

    <title>Reservation</title>
  </head>
  <body>
    <div style="width: 100%; height: 100%;">
        <div class="d-flex align-items-center justify-content-center" style="height: 100%;">
            <h2 class="brand">Reservation</h2>
        </div>
    </div>
  </body>
</html>